/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.io.alternative.test.parser;

import it.bewares.json.io.parser.JSONParser;
import it.bewares.json.io.alternative.parser.SimpleJSONParser;
//import it.bewares.json.test.io.parser.ParsingTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

/**
 * This test class is to test the alternative implemention of JSONParser
 * delivered with this project.
 *
 * @author Markus Goellnitz
 */
public class AlternativeParsingTest /*extends ParsingTest*/ {

    JSONParser parser;

    @Factory(dataProvider = "parsers")
    public AlternativeParsingTest(JSONParser parser) {
        //super(parser);
    }

    @DataProvider
    public static Object[][] parsers() {
        return new Object[][]{
            {new SimpleJSONParser()}
        };
    }

}
